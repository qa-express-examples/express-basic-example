const express = require('express');

let itemHistory = [];

app = express();

// JSON middleware to parse body.
app.use(express.json());


app.get('/test', (req, res) => {
    res.send('example GET');
});

app.get('/param/:exampleParam', (req, res) => {
    res.send('example GET with param. Param value: ' + req.params.exampleParam);
});

app.get('/query', (req, res) => {
    res.send('example GET with query params. example values: ' + req.query.example);
});

app.post('/post', (req, res) => {
    res.send(req.body);
});

app.get('/add/:item', (req, res) => {
    itemHistory.push(req.params.item);
    res.send(itemHistory);
});


app.listen(5000, () => {
    console.log('Listening on port 5000');
});